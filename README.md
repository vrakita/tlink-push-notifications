# README #

Basic information about TLink push notifications

### Topics ###

List of all possible topics:

* ```/topics/user-{user_id}```
* ```/topics/tournament-{tournament_id}```
* ```/topics/user-{following_user_id}-followers```


### Types and Events ###

List of all possible types and their belonging events:

* ```message```
	* ```message_admin```
* ```tournament```
	* ```removed```
	* ```info```
* ```round```
	* ```created```
	* ```finished```
	* ```scored```

### Example ###
```json
{
	"to": "/topics/user-1",
	"priority": "high",
	"notification": {
		"title": "it can be NULL or string",
		"body": "Hello World!"
	},
	"data": {
		"type": "round",
		"event": "scored",
		"url": "http://url_key_is_optional.trust.me"
	}
}
```